Hello, Revolut!

First of all, I was really interested to look at a Revolut application in my phone from a different angle. It was a great homework task and I hope you will like my solution for an exchange module. So, this project was bootstrapped with [Create React App](https:/github.com/facebook/create-react-app). I chose this for a faster start
because this is a demo/prototype, for real product I would build project without "create react app" to keep it lighter.

For example,
you will find in my `package.json` devDeps a typescript dependency, which I am not using but it is required when starting a project
with `React Create APP`. Later speaking of typescript, I am writing typescript everyday but I thought it would be faster to provide you a solution with simple ReactJS. For types I was using [prop-types](https://github.com/facebook/prop-types).

Next, if we talk about the style components, my favourites are `CSS-IN-JS` but I found in this job description, that you are using module css, so I choose to style my project with this technology.

About solution, I kept design almost the same as in application, just changed few pieces which were only applicable for mobile application. Wrote this project as responsive and mobile first with `Flexbox`.

I am setting up and manipulating pockets' balances using redux store. I am also using `redux-saga` to call API for FX rate and storing response to redux store. Transactions, also, are kept in redux store but I am saving only last one transaction.

## Available Scripts

In the project directory, you can run:

### `npm run start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimises the build for the best performance.

### `npm run test`

Launches the test runner for all tests.

### `npm run test:watch`

Launches the test runner in the interactive watch mode.

### `npm run lint`

Launches the lint runner for all files and returns errors and warrning regarding to eslint rules.

### `npm run format`

Launches a formatter to format code like described rules in .prettierrc file.
