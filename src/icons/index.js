import React from 'react';
import { EurIcon } from './EurIcon';
import { GbpIcon } from './GbpIcon';
import { UsdIcon } from './UsdIcon';
import { MarkerIcon } from './MarkerIcon';
import { TransferIcon } from './TransferIcon';

export const Icon = (props) => {
  const { name } = props;
  switch (name) {
    case 'USD':
      return <UsdIcon />;
    case 'EUR':
      return <EurIcon />;
    case 'GBP':
      return <GbpIcon />;
    case 'Mark':
      return <MarkerIcon />;
    case 'Transfer':
      return <TransferIcon />
    default:
      return <EurIcon />;
  }
};
