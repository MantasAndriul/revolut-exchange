import React from 'react';

export const TransferIcon = () => {
  return (
    <svg viewBox="0 0 419.175 419.175" xmlns="http://www.w3.org/2000/svg" width="14" height="14">
      <path style={{ fill: "#396aff" }} d="m209.588 89.823-74.853-89.823-74.853 89.823h59.882v239.529h29.941v-239.529z" />
      <path style={{ fill: "#396aff" }} d="m299.411 329.352v-239.529h-29.941v239.529h-59.882l74.853 89.823 74.853-89.823z" />
    </svg>
  );
};