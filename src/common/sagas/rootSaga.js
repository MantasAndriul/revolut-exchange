import { all, fork } from 'redux-saga/effects';
import exchangesWatcher from '../../containers/Exchange/sagas/exchangeSaga';
import balanceWatcher from './balanceSaga';
import transactionWatcher from './transactionSaga';

export const rootSaga = function* root() {
  yield all([
    fork(exchangesWatcher),
    fork(balanceWatcher),
    fork(transactionWatcher),
  ]);
};
