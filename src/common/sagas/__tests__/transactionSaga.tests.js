import { takeLatest } from 'redux-saga/effects';
import '../../../setupTests';
import transactionWatcher, { addTransaction } from '../transactionSaga';

describe('SAGAS', () => {
  it('should dispatch action "ADD_TRANSACTION" ', () => {
    const generator = transactionWatcher();
    expect(generator.next().value).toEqual(
      takeLatest('ADD_TRANSACTION', addTransaction)
    );
    expect(generator.next().done).toBeTruthy();
  });
});
