import { takeLatest } from 'redux-saga/effects';
import '../../../setupTests';
import balanceWatcher, { updateBalance } from '../balanceSaga';

describe('SAGAS', () => {
  it('should dispatch action "UPDATE_BALANCE" ', () => {
    const generator = balanceWatcher();
    expect(generator.next().value).toEqual(
      takeLatest('UPDATE_BALANCE', updateBalance)
    );
    expect(generator.next().done).toBeTruthy();
  });
});
