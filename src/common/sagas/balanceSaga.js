import { put, takeLatest, select } from 'redux-saga/effects';
import {
  UPDATE_BALANCE,
  UPDATE_BALANCE_ERROR,
  UPDATE_BALANCE_PENDING,
  UPDATE_BALANCE_FULFILLED,
} from '../constants/balanceConstants';
import { getBalanceSelector } from '../selectors/balanceSelector';
import history from '../../history';

export function* updateBalance(action) {
  try {
    const balanceObject = yield select(getBalanceSelector);

    const updatedObj = {
      ...balanceObject,
      [action.transaction.from]: {
        balance:
          balanceObject &&
          (
            Number(balanceObject[action.transaction.from].balance) -
            Number(action.transaction.amountPay)
          ).toFixed(2),
      },
      [action.transaction.to]: {
        balance:
          balanceObject &&
          (
            Number(balanceObject[action.transaction.to].balance) +
            Number(action.transaction.amountGet)
          ).toFixed(2),
      },
    };

    yield put({ type: UPDATE_BALANCE_PENDING });
    if (updatedObj) {
      yield put({
        type: UPDATE_BALANCE_FULFILLED,
        payload: {
          balances: updatedObj,
        },
      });
    }
    history.push('/');
  } catch (error) {
    yield put({ type: UPDATE_BALANCE_ERROR });
    console.log(error);
  }
}

export default function* balanceWatcher() {
  yield takeLatest(UPDATE_BALANCE, updateBalance);
}
