import { put, takeLatest } from 'redux-saga/effects';
import {
  ADD_TRANSACTION_PENDING,
  ADD_TRANSACTION_ERROR,
  ADD_TRANSACTION_FULFILLED,
  ADD_TRANSACTION,
} from '../constants/transactionConstants';

export function* addTransaction(action) {
  try {
    yield put({ type: ADD_TRANSACTION_PENDING });

    if (action) {
      yield put({
        type: ADD_TRANSACTION_FULFILLED,
        payload: {
          transaction: action.transaction,
        },
      });
    }
  } catch (error) {
    yield put({ type: ADD_TRANSACTION_ERROR });
    console.log(error);
  }
}

export default function* transactionWatcher() {
  yield takeLatest(ADD_TRANSACTION, addTransaction);
}
