import reducer from '../transactionReducer';

const initialState = {
  transactions: {
    transactions: [],
  },
};

describe('REDUCER', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, initialState)).toEqual(initialState);
  });
});
