import reducer from '../balanceReducer';

const initialState = {
  balances: {
    EUR: {
      balance: 100.0,
    },
    GBP: {
      balance: 100.0,
    },
    USD: {
      balance: 100.0,
    },
  },
};

describe('REDUCER', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, initialState)).toEqual(initialState);
  });

  it('should handle "UPDATE_BALANCE_FULFILLED" action', () => {
    const mockData = [
      {
        EUR: {
          balance: 99.0,
        },
        GBP: {
          balance: 101.1,
        },
        USD: {
          balance: 100.0,
        },
      },
    ];
    expect(
      reducer(
        {},
        { type: 'UPDATE_BALANCE_FULFILLED', payload: { balances: mockData[0] } }
      )
    ).toEqual({ balances: mockData[0] });
  });
});
