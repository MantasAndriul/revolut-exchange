import {
  UPDATE_BALANCE_PENDING,
  UPDATE_BALANCE_FULFILLED,
} from '../constants/balanceConstants';

const initialState = {
  balances: {
    EUR: {
      balance: 100.0,
    },
    GBP: {
      balance: 100.0,
    },
    USD: {
      balance: 100.0,
    },
  },
};

const balanceReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_BALANCE_PENDING: {
      return initialState.balances;
    }
    case UPDATE_BALANCE_FULFILLED: {
      return {
        ...state.balance,
        balances: action.payload.balances,
      };
    }
    default:
      return state;
  }
};

export default balanceReducer;
