import { combineReducers } from 'redux';
import exchangeReducer from '../../containers/Exchange/reducers/exchangeReducer';
import balanceReducer from './balanceReducer';
import transactionReducer from './transactionReducer';

export const rootReducers = combineReducers({
  exchangeReducer,
  balanceReducer,
  transactionReducer,
});
