import {
  ADD_TRANSACTION_PENDING,
  ADD_TRANSACTION_FULFILLED,
} from '../constants/transactionConstants';

const initialState = {
  transactions: {
    transactions: [],
  },
};

const transactionReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TRANSACTION_PENDING: {
      return initialState.transactions;
    }
    case ADD_TRANSACTION_FULFILLED: {
      return {
        ...state.transactions,
        transactions: [action.payload.transaction],
      };
    }
    default:
      return state;
  }
};

export default transactionReducer;
