import * as actions from '../balanceActions';
describe('ACTIONS', () => {
  it('should create an action with correct type', () => {
    const expectedAction = {
      type: 'UPDATE_BALANCE',
    };
    expect(actions.updateBalance()).toEqual(expectedAction);
  });
});
