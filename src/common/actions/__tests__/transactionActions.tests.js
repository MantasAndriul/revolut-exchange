import * as actions from '../transactionActions';
describe('ACTIONS', () => {
  it('should create an action with correct type', () => {
    const expectedAction = {
      type: 'ADD_TRANSACTION',
    };
    expect(actions.addTransaction()).toEqual(expectedAction);
  });
});
