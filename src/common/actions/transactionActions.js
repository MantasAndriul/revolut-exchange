import { ADD_TRANSACTION } from '../constants/transactionConstants';

export function addTransaction(transaction) {
  return {
    type: ADD_TRANSACTION,
    transaction,
  };
}
