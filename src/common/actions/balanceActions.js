import { UPDATE_BALANCE } from '../constants/balanceConstants';

export function updateBalance(transaction) {
  return {
    type: UPDATE_BALANCE,
    transaction,
  };
}
