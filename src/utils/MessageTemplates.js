export const MessageTemplates = {
  EUR: {
    message: 'Exchanged to EUR',
  },
  GBP: {
    message: 'Exchanged to GBP',
  },
  USD: {
    message: 'Exchanged to USD',
  },
};
