export const PocketInfo = {
  EUR: {
    currencySign: '€',
    abbrev: 'EUR',
    name: 'Euro',
  },
  GBP: {
    name: 'British Pound',
    abbrev: 'GBP',
    currencySign: '£',
  },
  USD: {
    name: 'US Dollar',
    abbrev: 'USD',
    currencySign: '$',
  },
};
