import { shallow } from 'enzyme';
import React from 'react';
import '../../../setupTests';
import { Icon } from '../../../icons';
import { PocketInfo } from '../../../utils/PocketInfo';
import PocketPickerItem from '../PocketPickerItem';

describe('PocketPickerItem component', () => {
  let wrapper;

  it('renders PocketPickerItem ', () => {
    wrapper = shallow(
      <PocketPickerItem
        icon={<Icon name={PocketInfo['EUR'].abbrev} />}
        name={PocketInfo['EUR'].name}
        abbrev={PocketInfo['EUR'].abbrev}
        currencySign={PocketInfo['EUR'].currencySign}
        amount={100}
        setSelectedPocketInfo={() => {}}
        setShowModal={() => {}}
        isActive={true}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('should simulate div click', () => {
    const mockedFunction = jest.fn();
    const mockedPocketFunction = jest.fn();
    const wrapper = shallow(
      <PocketPickerItem
        setShowModal={mockedFunction}
        setSelectedPocketInfo={mockedPocketFunction}
      />
    );
    wrapper.find('.wrapper').simulate('click');
    expect(mockedFunction).toHaveBeenCalled();
  });
});
