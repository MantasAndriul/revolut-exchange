import React from 'react';
import styles from './PocketPickerItem.module.css';
import PropTypes from 'prop-types';
import { PocketInfo } from '../../utils/PocketInfo';
import { Icon } from '../../icons';

export default function PocketPickerItem({
  icon,
  name,
  abbrev,
  amount,
  currencySign,
  setSelectedPocketInfo,
  setShowModal,
  isActive,
}) {
  return (
    <>
      <div
        onClick={() => {
          setSelectedPocketInfo(PocketInfo[abbrev]);
          setShowModal(false);
        }}
        className={styles.wrapper}
      >
        <div className={styles.iconWrapper}>{icon}</div>
        {isActive && (
          <i className={styles.activeMarker}>
            <Icon name="Mark" />
          </i>
        )}
        <div className={styles.currencyDetailWrapper}>
          <div className={styles.currencyDetail}>
            <div className={styles.currencyName}>{name}</div>
            <div className={styles.abbreviation}>{abbrev}</div>
          </div>
        </div>
        <div className={styles.pocket}>
          <div>
            {currencySign}
            {amount}
          </div>
        </div>
      </div>
    </>
  );
}

PocketPickerItem.prototype = {
  setShowModal: PropTypes.any.isRequired,
  setSelectedPocketInfo: PropTypes.func.isRequired,
  icon: PropTypes.any.isRequired,
  name: PropTypes.string.isRequired,
  abbrev: PropTypes.string.isRequired,
  amount: PropTypes.number.isRequired,
  currencySign: PropTypes.string.isRequired,
  isActive: PropTypes.bool.isRequired,
};
