import React from 'react';
import PropTypes from 'prop-types';
import styles from './ExchangeButton.module.css';

export default function ExchangeButton({ text, onExchangeAction, isDisabled }) {
  return (
    <div className={styles.exchangeButtonWrapper}>
      <button
        disabled={isDisabled}
        onClick={() => onExchangeAction()}
        className={styles.exchangeButton}
      >
        {text}
      </button>
    </div>
  );
}

ExchangeButton.prototype = {
  text: PropTypes.string.isRequired,
  onExchangeAction: PropTypes.func.isRequired,
  isDisabled: PropTypes.bool.isRequired,
};
