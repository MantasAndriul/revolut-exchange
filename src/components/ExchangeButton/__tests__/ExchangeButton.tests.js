import React from 'react';
import { shallow } from 'enzyme';
import ExchangeButton from '../ExchangeButton';
import '../../../setupTests';

describe('Exchange button component', () => {
  let wrapper;

  it('renders correctly', () => {
    wrapper = shallow(
      <ExchangeButton disabled={true} onClick={() => {}} text="test" />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('should simulate div click', () => {
    const mockedFunction = jest.fn();
    const wrapper = shallow(
      <ExchangeButton onExchangeAction={mockedFunction} />
    );
    wrapper.find('button').simulate('click');
    expect(mockedFunction).toHaveBeenCalled();
  });
});
