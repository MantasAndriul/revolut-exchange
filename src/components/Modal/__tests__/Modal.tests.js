import { shallow } from 'enzyme';
import Modal from '../Modal';
import React from 'react';
import ReactModal from 'react-modal';
import '../../../setupTests';
import { PocketInfo } from '../../../utils/PocketInfo';

const state = {
  balanceReducer: {
    balances: {
      EUR: {
        balance: 100,
      },
      GBP: {
        balance: 100,
      },
      USD: {
        balance: 100,
      },
    },
  },
  transactionReducer: {
    transactions: [],
  },
};

describe('Modal component', () => {
  let wrapper;

  it('renders react-modal', () => {
    wrapper = shallow(
      <Modal
        showModal={true}
        setShowModal={() => {}}
        selectedPocketInfo={PocketInfo['EUR']}
        setSelectedPocketInfo={() => {}}
        balances={state.balanceReducer.balances}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('renders react-modal', () => {
    const wrapper = shallow(<Modal />);
    expect(wrapper.find(ReactModal)).toHaveLength(1);
  });
});
