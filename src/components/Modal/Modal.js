import React from 'react';
import ReactModal from 'react-modal';
import PropTypes from 'prop-types';
import { Icon } from '../../icons/index';
import styles from './Modal.module.css';
import PocketPickerItem from '../PocketPickerItem/PocketPickerItem';
import { PocketInfo } from '../../utils/PocketInfo';

export default function Modal({
  setShowModal,
  showModal,
  selectedPocketInfo,
  setSelectedPocketInfo,
  balances,
  filterItem,
}) {
  return (
    <>
      <ReactModal
        isOpen={Boolean(showModal)}
        onRequestClose={() => setShowModal(false)}
        shouldCloseOnOverlayClick={true}
        className={styles.modal}
        overlayClassName={styles.overlay}
        ariaHideApp={false}
      >
        <div className={styles.closeIcon} onClick={() => setShowModal(false)}>
          X
        </div>
        {Object.keys(PocketInfo)
          .filter((key) => PocketInfo[key].abbrev !== filterItem)
          .map((key) => (
            <PocketPickerItem
              key={key}
              icon={<Icon name={PocketInfo[key].abbrev} />}
              name={PocketInfo[key].name}
              abbrev={PocketInfo[key].abbrev}
              currencySign={PocketInfo[key].currencySign}
              amount={balances && balances[PocketInfo[key].abbrev].balance}
              setSelectedPocketInfo={setSelectedPocketInfo}
              setShowModal={setShowModal}
              isActive={
                selectedPocketInfo
                  ? PocketInfo[key].abbrev === selectedPocketInfo.abbrev
                  : false
              }
            />
          ))}
      </ReactModal>
    </>
  );
}

Modal.prototype = {
  setShowModal: PropTypes.any.isRequired,
  showModal: PropTypes.bool.isRequired,
  selectedPocketInfo: PropTypes.object.isRequired,
  setSelectedPocketInfo: PropTypes.func.isRequired,
  balances: PropTypes.object.isRequired,
  filterItem: PropTypes.string,
};
