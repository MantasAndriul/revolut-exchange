import React from 'react';
import PropTypes from 'prop-types';
import styles from './SelectedPocketItem.module.css';
import { Icon } from '../../icons';

export default function SelectedPocketItem({
  setShowModal,
  selectedPocketInfo,
  balances,
}) {
  return (
    <>
      <div className={styles.frontRowWrapper}>
        <div className={styles.currentPocketInfo}>
          <div onClick={() => setShowModal(true)} className={styles.pocketName}>
            {selectedPocketInfo.currencySign}{' '}
            {balances[selectedPocketInfo.abbrev].balance}{' '}
            <i className={styles.arrowDown}></i>
          </div>
        </div>
        <div className={styles.iconWrapper}>
          <Icon name={selectedPocketInfo.abbrev} />
        </div>
      </div>
      <div className={styles.currencyName}>{selectedPocketInfo.name}</div>
    </>
  );
}

SelectedPocketItem.prototype = {
  setShowModal: PropTypes.any.isRequired,
  balances: PropTypes.any.isRequired,
  selectedPocketInfo: PropTypes.object.isRequired,
};
