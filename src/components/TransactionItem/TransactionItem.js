import React from 'react';
import PropTypes from 'prop-types';
import styles from './TransactionItem.module.css';
import { ExchangeIcon } from '../../icons/ExchangeIcon';
import { MessageTemplates } from '../../utils/MessageTemplates';
import { PocketInfo } from '../../utils/PocketInfo';
import moment from 'moment';

export default function TransactionItem(props) {
  const { transactions } = props;
  const transactionDate = moment(transactions.date)
    .calendar()
    .replace(' at ', ', ');
  return (
    <>
      <div className={styles.transactionWrapper}>
        <div className={styles.transIconWrapper}>
          <div className={styles.circle}>
            <ExchangeIcon color={'#407bbf'} size={'20px'} />
          </div>
        </div>
        <div className={styles.transItemWrapper}>
          <div className={styles.transItemText}>
            <div className={styles.templateText}>
              {MessageTemplates[transactions.to].message}
            </div>
            <div className={styles.transTime}>{transactionDate}</div>
          </div>
        </div>
        <div className={styles.transItemText}>
          <div>
            + {PocketInfo[transactions.to].currencySign}
            {transactions.amountGet}
          </div>
          <div className={styles.exchangedAmountText}>
            - {PocketInfo[transactions.from].currencySign}
            {transactions.amountPay}
          </div>
        </div>
      </div>
    </>
  );
}

TransactionItem.prototype = {
  transactions: PropTypes.array.isRequired,
};
