import React from 'react';
import { shallow } from 'enzyme';
import '../../../setupTests';
import TransactionItem from '../TransactionItem';

const state = {
  transactionReducer: {
    transactions: [
      {
        to: 'GBP',
        from: 'EUR',
        amountGet: '0.9',
        amountPay: '1',
        date: new Date(),
      },
    ],
  },
};

describe('TransactionItem component', () => {
  let wrapper;

  it('renders correctly', () => {
    wrapper = shallow(
      <TransactionItem
        transactions={state.transactionReducer.transactions[0]}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
