import React from 'react';
import PropTypes from 'prop-types';
import styles from './ExchangeItem.module.css';
import Input from '../Input/Input';

export default function ExchangeItem({
  abbrev,
  balance,
  currencySign,
  setShowModal,
  isDisabled,
  exchangeValue,
  inputValue,
  setInputValue,
}) {
  return (
    <>
      <div className={styles.wrapper}>
        <div>
          <div
            className={styles.currencyText}
            onClick={() => setShowModal(true)}
          >
            {abbrev} <i className={styles.arrowDown}></i>
          </div>
          <div className={styles.balanceText}>
            Balance: {currencySign} {balance}
          </div>
        </div>
        <Input
          isDisabled={isDisabled}
          exchangeValue={exchangeValue}
          inputValue={inputValue}
          setInputValue={setInputValue}
        />
      </div>
    </>
  );
}

ExchangeItem.prototype = {
  setShowModal: PropTypes.any.isRequired,
  inputValue: PropTypes.any.isRequired,
  setInputValue: PropTypes.func.isRequired,
  exchangeValue: PropTypes.any.isRequired,
  abbrev: PropTypes.string.isRequired,
  balance: PropTypes.number.isRequired,
  currencySign: PropTypes.string.isRequired,
  isDisabled: PropTypes.bool.isRequired,
};
