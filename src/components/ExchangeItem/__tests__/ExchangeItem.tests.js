import React from 'react';
import { shallow } from 'enzyme';
import { PocketInfo } from '../../../utils/PocketInfo';
import '../../../setupTests';
import ExchangeItem from '../ExchangeItem';

describe('Exchange item component', () => {
  let wrapper;

  it('renders correctly', () => {
    wrapper = shallow(
      <ExchangeItem
        setShowModal={() => {}}
        balance={100}
        inputValue={''}
        setInputValue={() => {}}
        exchangeValue={0}
        abbrev={PocketInfo['EUR'].abbrev}
        currencySign={PocketInfo['EUR']}
        isDisabled={true}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('should simulate div click', () => {
    const mockedFunction = jest.fn();
    const wrapper = shallow(<ExchangeItem setShowModal={mockedFunction} />);
    wrapper.find('.currencyText').simulate('click');
    expect(mockedFunction).toHaveBeenCalled();
  });
});
