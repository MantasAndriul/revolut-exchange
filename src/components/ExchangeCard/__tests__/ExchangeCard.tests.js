import React from 'react';
import { shallow } from 'enzyme';
import { PocketInfo } from '../../../utils/PocketInfo';
import '../../../setupTests';
import ExchangeCard from '../ExchangeCard';

const state = {
  balanceReducer: {
    balances: {
      EUR: {
        balance: 100,
      },
      GBP: {
        balance: 100,
      },
      USD: {
        balance: 100,
      },
    },
  },
  transactionReducer: {
    transactions: [],
  },
};

describe('Exchange card component', () => {
  let wrapper;

  it('renders correctly', () => {
    wrapper = shallow(
      <ExchangeCard
        setShowModal={() => {}}
        balances={state.balanceReducer.balances}
        exchangePocketInfo={PocketInfo['GBP']}
        setModalType={() => {}}
        rate={0.9101}
        inputValue={''}
        selectPocketInfo={PocketInfo['EUR']}
        setInputValue={() => {}}
        exchangeValue={0}
        onTransactionAction={() => {}}
        changePockets={() => {}}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
