import React from 'react';
import PropTypes from 'prop-types';
import styles from './ExchangeCard.module.css';
import ExchangeItem from '../ExchangeItem/ExchangeItem';
import RateSeparator from '../RateSeparator/RateSeparator';
import ExchangeButton from '../ExchangeButton/ExchangeButton';

export default function ExchangeCard({
  setShowModal,
  selectPocketInfo,
  exchangePocketInfo,
  setModalType,
  rate,
  balances,
  inputValue,
  setInputValue,
  exchangeValue,
  onTransactionAction,
  changePockets,
}) {
  return (
    <section className={styles.cards}>
      <div className={styles.card}>
        <div className={styles.cardContent}>
          <div onClick={() => setModalType('select')}>
            <ExchangeItem
              abbrev={selectPocketInfo.abbrev}
              balance={balances && balances[selectPocketInfo.abbrev].balance}
              currencySign={selectPocketInfo.currencySign}
              setShowModal={setShowModal}
              isDisabled={false}
              inputValue={inputValue}
              setInputValue={setInputValue}
            />
          </div>
          <RateSeparator
            abbrevExchCurr={exchangePocketInfo.abbrev}
            currencySign={selectPocketInfo.currencySign}
            rate={rate && rate.toFixed(4)}
            changePockets={changePockets}
            currSelected={selectPocketInfo}
            currExchange={exchangePocketInfo}
          />
          <div onClick={() => setModalType('exchange')}>
            <ExchangeItem
              abbrev={exchangePocketInfo.abbrev}
              balance={balances && balances[exchangePocketInfo.abbrev].balance}
              currencySign={exchangePocketInfo.currencySign}
              setShowModal={setShowModal}
              isDisabled={true}
              exchangeValue={exchangeValue}
            />
          </div>
          <ExchangeButton
            text={'Exchange'}
            onExchangeAction={onTransactionAction}
            isDisabled={
              (balances &&
                Number(balances[selectPocketInfo.abbrev].balance) <
                  inputValue) ||
              !inputValue
            }
          />
        </div>
      </div>
    </section>
  );
}

ExchangeCard.prototype = {
  setShowModal: PropTypes.any.isRequired,
  selectedPocketInfo: PropTypes.object.isRequired,
  balances: PropTypes.object.isRequired,
  exchangePocketInfo: PropTypes.object.isRequired,
  setModalType: PropTypes.bool.isRequired,
  rate: PropTypes.number.isRequired,
  inputValue: PropTypes.any.isRequired,
  setInputValue: PropTypes.func.isRequired,
  exchangeValue: PropTypes.any.isRequired,
  onTransactionAction: PropTypes.func.isRequired,
  changePockets: PropTypes.func.isRequired,
};
