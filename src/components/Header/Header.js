import React from 'react';
import PropTypes from 'prop-types';
import styles from './Header.module.css';
import { Link } from 'react-router-dom';
import { ExchangeIcon } from '../../icons/ExchangeIcon';

export default function Header({ name }) {
  return (
    <section className={styles.header}>
      <div className={styles.headerItem}>{name}</div>
      <div className={styles.headerItem}>
        {name === 'Home' ? (
          <Link className={styles.linkStyle} to={'/exchange'}>
            <ExchangeIcon color={'black'} size={'30px'} />{' '}
          </Link>
        ) : (
          <Link className={styles.linkStyle} to={'/'}>
            &lt; Back
          </Link>
        )}
      </div>
    </section>
  );
}

Header.prototype = {
  name: PropTypes.string.isRequired,
};
