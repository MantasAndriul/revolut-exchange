import React from 'react';
import { shallow } from 'enzyme';
import Header from '../Header';
import '../../../setupTests';

describe('Header component', () => {
  let wrapper;

  it('renders correctly', () => {
    wrapper = shallow(<Header name="Home" />);
    expect(wrapper).toMatchSnapshot();
  });
});
