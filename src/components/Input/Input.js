import React from 'react';
import PropTypes from 'prop-types';
import styles from './Input.module.css';

export default function Input({
  isDisabled,
  exchangeValue,
  inputValue,
  setInputValue,
}) {
  const validate = (e) => {
    if (e.keyCode === 190 && e.target.value.split('.').length === 2) {
      e.preventDefault();
    }
    if (
      e.keyCode !== 8 &&
      e.target.value.includes('.') &&
      e.target.value.split('.')[1].length === 2
    ) {
      e.preventDefault();
    }
    if (['e', 'E', '+', '-'].includes(e.key)) {
      e.preventDefault();
    }
    if (e.keyCode !== 8 && e.target.value.length > 8) {
      e.preventDefault();
    }
    if (e.target.value.length === 0 && e.keyCode === 48) {
      e.preventDefault();
    }
  };

  return (
    <>
      <div className={styles.inputWrapper}>
        <input
          className={styles.input}
          value={
            exchangeValue || exchangeValue === 0 ? exchangeValue : inputValue
          }
          onKeyDown={(e) => validate(e)}
          onChange={(e) => setInputValue(e.target.value)}
          disabled={isDisabled}
          type="number"
          placeholder={0}
        />
      </div>
    </>
  );
}

Input.prototype = {
  inputValue: PropTypes.any,
  setInputValue: PropTypes.func,
  exchangeValue: PropTypes.any,
  isDisabled: PropTypes.bool.isRequired,
};
