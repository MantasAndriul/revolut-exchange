import React from 'react';
import { shallow, mount } from 'enzyme';
import '../../../setupTests';
import Input from '../Input';

describe('Input component', () => {
  let wrapper;

  it('renders correctly', () => {
    wrapper = shallow(
      <Input inputValue={2} setInputValue={() => {}} isDisabled={false} />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('should call onChange prop', () => {
    const onEnterMock = jest.fn();
    const event = {
      preventDefault() {},
      target: { value: '12' },
    };
    const component = shallow(<Input setInputValue={onEnterMock} />);
    component.find('input').simulate('change', event);
    expect(onEnterMock).toBeCalledWith('12');
  });

  it('should call onChange prop with input value', () => {
    const onEnterMock = jest.fn();
    const component = mount(
      <Input setInputValue={onEnterMock} value="string value" />
    );
    component.find('input').simulate('change');
    expect(onEnterMock).toBeCalledWith('');
  });
});
