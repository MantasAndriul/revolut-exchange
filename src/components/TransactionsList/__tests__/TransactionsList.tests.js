import React from 'react';
import { shallow } from 'enzyme';
import '../../../setupTests';
import TransactionsList from '../TransactionsList';

const state = {
  transactionReducer: {
    transactions: [
      {
        to: 'GBP',
        from: 'EUR',
        amountGet: '0.9',
        amountPay: '1',
        date: new Date(),
      },
    ],
  },
};

describe('TransactionsList component', () => {
  let wrapper;

  it('renders correctly', () => {
    wrapper = shallow(
      <TransactionsList
        transactions={state.transactionReducer.transactions[0]}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
