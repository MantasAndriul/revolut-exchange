import React from 'react';
import PropTypes from 'prop-types';
import styles from './TransactionsList.module.css';
import TransactionItem from '../TransactionItem/TransactionItem';

export default function TransactionsList(props) {
  const { transactions } = props;

  return (
    <>
      <p className={styles.transactions}>Transactions </p>
      {transactions &&
        transactions.length > 0 &&
        transactions.map((trans, i) => (
          <TransactionItem key={i} transactions={trans} />
        ))}
    </>
  );
}

TransactionsList.prototype = {
  transactions: PropTypes.array.isRequired,
};
