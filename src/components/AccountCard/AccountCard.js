import React from 'react';
import PropTypes from 'prop-types';
import styles from './AccountCard.module.css';
import { Link } from 'react-router-dom';
import { ExchangeIcon } from '../../icons/ExchangeIcon';
import TransactionsList from '../TransactionsList/TransactionsList';
import SelectedPocketItem from '../SelectedPocketItem/SelectedPocketItem';

export default function AccountCard({
  setShowModal,
  selectedPocketInfo,
  balances,
  transactions,
}) {
  return (
    <section className={styles.cards}>
      <div className={styles.card}>
        <div className={styles.cardContent}>
          <SelectedPocketItem
            setShowModal={setShowModal}
            selectedPocketInfo={selectedPocketInfo}
            balances={balances}
          />
          <div className={styles.menuButtonWrapper}>
            <Link to={'/exchange'} className={styles.button}>
              {' '}
              <ExchangeIcon color={'#396aff'} size={'16px'} />{' '}
              <span className={styles.insideButton}>Exchange</span>{' '}
            </Link>
          </div>
          <TransactionsList transactions={transactions} />
        </div>
      </div>
    </section>
  );
}

AccountCard.prototype = {
  setShowModal: PropTypes.any.isRequired,
  selectedPocketInfo: PropTypes.object.isRequired,
  balances: PropTypes.object.isRequired,
  transactions: PropTypes.array.isRequired,
};
