import React from 'react';
import { shallow } from 'enzyme';
import AccountCard from '../AccountCard';
import { PocketInfo } from '../../../utils/PocketInfo';
import '../../../setupTests';

const state = {
  balanceReducer: {
    balances: {
      EUR: {
        balance: 100,
      },
      GBP: {
        balance: 100,
      },
      USD: {
        balance: 100,
      },
    },
  },
  transactionReducer: {
    transactions: [],
  },
};

describe('Account card component', () => {
  let wrapper;

  it('renders correctly', () => {
    wrapper = shallow(
      <AccountCard
        setShowModal={() => {}}
        selectedPocketInfo={PocketInfo['EUR']}
        balances={state.balanceReducer.balances}
        transactions={state.transactionReducer}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
