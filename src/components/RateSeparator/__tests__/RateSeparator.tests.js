import React from 'react';
import { shallow } from 'enzyme';
import { PocketInfo } from '../../../utils/PocketInfo';
import '../../../setupTests';
import RateSeparator from '../RateSeparator';

describe('RateSeparator component', () => {
  let wrapper;

  it('renders correctly', () => {
    wrapper = shallow(
      <RateSeparator
        abbrevExchCurr={'GBP'}
        currencySign={'EUR'}
        rate={0.9101}
        changePockets={() => {}}
        currSelected={PocketInfo['EUR']}
        currExchange={PocketInfo['GBP']}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('should simulate div click', () => {
    const mockedFunction = jest.fn();
    const wrapper = shallow(<RateSeparator changePockets={mockedFunction} />);
    wrapper.find('.changeCircle').simulate('click');
    expect(mockedFunction).toHaveBeenCalled();
  });
});
