import React from 'react';
import PropTypes from 'prop-types';
import styles from './RateSeparator.module.css';
import { Icon } from '../../icons';

export default function RateSeparator({
  rate,
  abbrevExchCurr,
  currencySign,
  changePockets,
  currSelected,
  currExchange,
}) {
  return (
    <>
      <div
        onClick={() => changePockets(currSelected, currExchange)}
        className={styles.changeCircle}
      >
        <div className={styles.transferIcon}><Icon name="Transfer" /></div>
      </div>
      <div className={styles.line} />
      <div className={styles.circle}>
        <div className={styles.rateText}>
          {currencySign}1 = {abbrevExchCurr} {rate}
        </div>
      </div>
    </>
  );
}

RateSeparator.prototype = {
  rate: PropTypes.number.isRequired,
  abbrevExchCurr: PropTypes.string.isRequired,
  currSelected: PropTypes.string.isRequired,
  currExchange: PropTypes.string.isRequired,
  changePockets: PropTypes.func.isRequired,
  currencySign: PropTypes.string.isRequired,
};
