import React from 'react';
import { connect } from 'react-redux';
import Header from '../../components/Header/Header';
import AccountCard from '../../components/AccountCard/AccountCard';
import Modal from '../../components/Modal/Modal';
import { PocketInfo } from '../../utils/PocketInfo';
import { getBalanceSelector } from '../../common/selectors/balanceSelector';
import { getTransactionSelector } from '../../common/selectors/transactionSelector';

function HomeContainer({ balances, transactions }) {
  const [showModal, setShowModal] = React.useState(false);
  const [selectedPocketInfo, setSelectedPocketInfo] = React.useState(
    PocketInfo.EUR
  );

  return (
    <>
      <Header name={'Home'} />
      <AccountCard
        setShowModal={setShowModal}
        selectedPocketInfo={selectedPocketInfo}
        balances={balances}
        transactions={transactions}
      />
      <Modal
        showModal={showModal}
        setShowModal={setShowModal}
        selectedPocketInfo={selectedPocketInfo}
        setSelectedPocketInfo={setSelectedPocketInfo}
        balances={balances}
      />
    </>
  );
}

const mapStateToProps = (state) => ({
  balances: getBalanceSelector(state),
  transactions: getTransactionSelector(state),
});

export default connect(mapStateToProps, null)(HomeContainer);
