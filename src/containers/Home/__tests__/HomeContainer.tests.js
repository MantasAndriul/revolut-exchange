import React from 'react';
import { Provider } from 'react-redux';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import HomeContainer from '../HomeContainer';
import '../../../setupTests';

describe('HomeContainer container', () => {
  let wrapper;

  it('should render correctly <HomeContainer>', () => {
    const mockStore = configureStore();
    const store = mockStore({
      balanceReducer: {
        balances: {
          EUR: {
            balance: 100,
          },
          GBP: {
            balance: 100,
          },
          USD: {
            balance: 100,
          },
        },
      },
      transactionReducer: {
        transactions: [],
      },
    });
    wrapper = shallow(
      <Provider store={store}>
        <HomeContainer />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
