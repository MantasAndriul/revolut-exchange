import { GET_RATE } from '../constants/actionTypes';

export function getRate(baseCurr, exchangeCurr) {
  return {
    type: GET_RATE,
    baseCurr,
    exchangeCurr,
  };
}
