import reducer from '../reducers/exchangeReducer';

const initialState = {
  rate: {
    rate: 0,
  },
};

describe('REDUCER', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, initialState)).toEqual(initialState);
  });

  it('should handle "GET_RATE_FULFILLED" action', () => {
    const mockData = [
      {
        rate: 0.9211,
      },
    ];
    expect(
      reducer({}, { type: 'GET_RATE_FULFILLED', payload: { rate: 0.9211 } })
    ).toEqual({ rate: mockData[0] });
  });
});
