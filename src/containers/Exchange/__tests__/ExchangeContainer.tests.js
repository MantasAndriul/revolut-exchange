import React from 'react';
import { Provider } from 'react-redux';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import ExchangeContainer from '../ExchangeContainer';
import '../../../setupTests';

describe('ExchangeContainer container', () => {
  let wrapper;

  it('should render correctly <ExchangeContainer>', () => {
    const mockStore = configureStore();
    const store = mockStore({
      balanceReducer: {
        balances: {
          EUR: {
            balance: 100,
          },
          GBP: {
            balance: 100,
          },
          USD: {
            balance: 100,
          },
        },
      },
      transactionReducer: {
        transactions: [],
      },
    });
    wrapper = shallow(
      <Provider store={store}>
        <ExchangeContainer />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
