import * as actions from '../actions/exchangeActions';
describe('ACTIONS', () => {
  it('should create an action with correct type', () => {
    const expectedAction = {
      type: 'GET_RATE',
    };
    expect(actions.getRate()).toEqual(expectedAction);
  });
});
