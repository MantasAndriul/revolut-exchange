import { takeLatest } from 'redux-saga/effects';
import '../../../setupTests';
import exchangesWatcher, { getLiveRate } from '../sagas/exchangeSaga';

describe('SAGAS', () => {
  it('should dispatch action "GET_RATE" ', () => {
    const generator = exchangesWatcher();
    expect(generator.next().value).toEqual(takeLatest('GET_RATE', getLiveRate));
    expect(generator.next().done).toBeTruthy();
  });
});
