import React from 'react';
import { connect } from 'react-redux';
import Header from '../../components/Header/Header';
import ExchangeCard from '../../components/ExchangeCard/ExchangeCard';
import Modal from '../../components/Modal/Modal';
import { PocketInfo } from '../../utils/PocketInfo';
import { getRate } from './actions/exchangeActions';
import { getExchangeSelector } from './selectors/exchangeSelector';
import { getBalanceSelector } from '../../common/selectors/balanceSelector';
import { updateBalance } from '../../common/actions/balanceActions';
import { addTransaction } from '../../common/actions/transactionActions';

function ExchangeContainer({
  rate,
  balances,
  getRate,
  updateBalance,
  addTransaction,
}) {
  const [showModal, setShowModal] = React.useState(false);
  const [modalType, setModalType] = React.useState('select');
  const [selectPocketInfo, setSelectedPocketInfo] = React.useState(
    PocketInfo.EUR
  );
  const [exchangePocketInfo, setExchangePocketInfo] = React.useState(
    PocketInfo.GBP
  );
  const [inputValue, setInputValue] = React.useState('');
  const [exchangeValue, setExchangeValue] = React.useState(0);

  const baseCurr = selectPocketInfo.abbrev;
  const exchangeCurr = exchangePocketInfo.abbrev;

  React.useEffect(() => {
    if (!inputValue.length) {
      setExchangeValue(0);
    }
    const conversion = rate && rate * inputValue;
    setExchangeValue(conversion && conversion.toFixed(2));
  }, [inputValue, rate]);

  React.useEffect(() => {
    getRate(baseCurr, exchangeCurr);
  }, [getRate, baseCurr, exchangeCurr]);

  const transactionAction = () => {
    const transactionObject = {
      from: selectPocketInfo.abbrev,
      to: exchangePocketInfo.abbrev,
      amountPay: inputValue,
      amountGet: exchangeValue,
      date: new Date(),
    };

    updateBalance(transactionObject);
    addTransaction(transactionObject);
  };

  const changePockets = (prevSelected, prevExchage) => {
    setSelectedPocketInfo(prevExchage);
    setExchangePocketInfo(prevSelected);
  };

  return (
    <>
      <Header name={'Exchange'} />
      <ExchangeCard
        setShowModal={setShowModal}
        selectPocketInfo={selectPocketInfo}
        exchangePocketInfo={exchangePocketInfo}
        setModalType={setModalType}
        rate={rate}
        balances={balances}
        inputValue={inputValue}
        setInputValue={setInputValue}
        exchangeValue={exchangeValue}
        onTransactionAction={transactionAction}
        changePockets={changePockets}
      />
      <Modal
        showModal={showModal}
        setShowModal={setShowModal}
        setSelectedPocketInfo={
          modalType === 'select' ? setSelectedPocketInfo : setExchangePocketInfo
        }
        filterItem={modalType === 'select' ? exchangeCurr : baseCurr}
        balances={balances}
      />
    </>
  );
}

const mapStateToProps = (state) => ({
  rate: getExchangeSelector(state),
  balances: getBalanceSelector(state),
});

const mapDispatchToProps = (dispatch) => ({
  getRate: (baseCurr, exchangeCurr) =>
    dispatch(getRate(baseCurr, exchangeCurr)),
  updateBalance: (transaction) => dispatch(updateBalance(transaction)),
  addTransaction: (transaction) => dispatch(addTransaction(transaction)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ExchangeContainer);
