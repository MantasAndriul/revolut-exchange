import { put, takeLatest, delay } from 'redux-saga/effects';
import {
  GET_RATE,
  GET_RATE_PENDING,
  GET_RATE_FULFILLED,
  GET_RATE_ERROR,
} from '../constants/actionTypes';

export function* getLiveRate(a) {
  while (true) {
    try {
      const requestURL = `https://api.exchangeratesapi.io/latest?base=${a.baseCurr}&symbols=${a.exchangeCurr}`;
      const rate = yield fetch(requestURL).then((response) => response.json());
      yield put({ type: GET_RATE_PENDING });
      if (rate) {
        const liveRate = rate.rates[a.exchangeCurr];
        yield put({
          type: GET_RATE_FULFILLED,
          payload: {
            rate: liveRate,
          },
        });
      }
      yield delay(10000);
    } catch (error) {
      yield put({ type: GET_RATE_ERROR });
      console.log(error);
    }
  }
}

export default function* exchangesWatcher() {
  yield takeLatest(GET_RATE, getLiveRate);
}
