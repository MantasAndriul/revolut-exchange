import { GET_RATE_PENDING, GET_RATE_FULFILLED } from '../constants/actionTypes';

const initialState = {
  rate: {
    rate: 0,
  },
};

const exchangeReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_RATE_PENDING: {
      return initialState.rate;
    }
    case GET_RATE_FULFILLED: {
      return {
        ...state.rate,
        rate: {
          rate: action.payload.rate,
        },
      };
    }
    default:
      return state;
  }
};

export default exchangeReducer;
